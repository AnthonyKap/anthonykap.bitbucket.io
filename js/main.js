var lng = 0;
var lat = 0;

var response = "";

//r=300 omdat test
const asyncWithJquery = () => {
  $.get("https://transit.land/api/v1/stops?lat=-22.91210&lon=-43.22919&r=300")
    .done(asycnSuccessCallbackHandler)
    .fail(asycnFailCallbackHandler);
}

//jquery call is successful
const asycnSuccessCallbackHandler = (data) => {
  response == data;
  console.log(data);
  console.log(data.stops[0].geometry.coordinates);
  lat = data.stops[0].geometry.coordinates[0];
  lng = data.stops[0].geometry.coordinates[1];
  console.log(lat);
  console.log(lng);
}

//jquery call failure
const asycnFailCallbackHandler = (data) => {
  console.log("werkt niet jammer");
}

asyncWithJquery();

function initMap() {
  var uluru = { lat, lng };
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 16,
    center: uluru
  });
  var marker = new google.maps.Marker({
    position: uluru,
    map: map
  });

  //Eigen Locatie
  var infoWindow = new google.maps.InfoWindow;
  var service = new google.maps.places.PlacesService(map);
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function (position) {
      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };

      infoWindow.setPosition(pos);
      infoWindow.setContent('Your location');
      infoWindow.open(map);
      map.setCenter(pos);
      service.nearbySearch({
        location: (pos),
        radius: 500,
        type: ['transit_station']
      }, callback);

    },
      function () {
        handleLocationError(true, infoWindow, map.getCenter());
      });
  } else {
    // Geen locatie
    handleLocationError(false, infoWindow, map.getCenter());
  }
  function callback(results, status) {
    if (status === google.maps.places.PlacesServiceStatus.OK) {
      for (var i = 0; i < results.length; i++) {
        createMarker(results[i]);
      }
    }
  }

  function createMarker(place) {
    var placeLoc = place.geometry.location;
    var marker = new google.maps.Marker({
      map: map,
      position: place.geometry.location
    });

    google.maps.event.addListener(marker, 'click', function () {
      infowindow.setContent(place.name);
      infowindow.open(map, this);
    });
  }
}